import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutAdminComponent } from './layout-admin/layout-admin.component';
import { Routes, Router, RouterModule } from '@angular/router';
import { QuanlydonhangComponent } from './layout-admin/quanlydonhang/quanlydonhang.component';
import { QuanlynguoidungComponent } from './layout-admin/quanlynguoidung/quanlynguoidung.component';
import { QuanlysanphamComponent } from './layout-admin/quanlysanpham/quanlysanpham.component';
import { AdminHomeComponent } from './layout-admin/admin-home/admin-home.component';
const adminRoute:Routes = [
  {
    path:'',component:LayoutAdminComponent,children:[
      {path:'',component:AdminHomeComponent},
      {path:'QuanLyDonHang',component:QuanlydonhangComponent},
      {path:'QuanLyNguoiDung',component:QuanlynguoidungComponent},
      {path:'QuanLySanPham',component:QuanlysanphamComponent},
      {path:'**',redirectTo:''}
    ],
    
  }
  
];  
@NgModule({
  imports: [
    CommonModule,RouterModule.forChild(adminRoute),
  ],
  declarations: [LayoutAdminComponent,QuanlydonhangComponent,QuanlynguoidungComponent,QuanlysanphamComponent,AdminHomeComponent]
})
export class AdminModule { }
