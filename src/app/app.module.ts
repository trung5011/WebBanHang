import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { AdminModule } from './admin/admin.module';
import { HomeModule } from './home/home.module';
import { appRoutes } from './routes/app.routes';
import {BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AnimationComponent } from './component/animation/animation.component';

@NgModule({
  declarations: [
    AppComponent,
    AnimationComponent
  ],
  imports: [
    BrowserModule,AdminModule,HomeModule,RouterModule,appRoutes
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
