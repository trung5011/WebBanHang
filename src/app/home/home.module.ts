import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TrangChuComponent } from './trang-chu/trang-chu.component';
import { TrangSanPhamComponent } from './trang-san-pham/trang-san-pham.component';
import { TrangDanhSachSanPhamComponent } from './trang-danh-sach-san-pham/trang-danh-sach-san-pham.component';
import { GioHangComponent } from './gio-hang/gio-hang.component';
import { Routes,RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
const homeRoute:Routes = [
  {
    path:'',component:HomeComponent,children:[
      {path:'',component:TrangChuComponent},
      {path:'danhsachsanpham',component:TrangDanhSachSanPhamComponent},
      {path:'sanpham',component:TrangSanPhamComponent},
      {path:'giohang',component:GioHangComponent},
      {path:'**',redirectTo:''}
    ],
    
  }
  
];  
@NgModule({
  imports: [
    CommonModule,RouterModule.forChild(homeRoute)
  ],
  declarations: [TrangChuComponent, TrangSanPhamComponent, TrangDanhSachSanPhamComponent, GioHangComponent, HomeComponent]
})
export class HomeModule { }
