import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrangDanhSachSanPhamComponent } from './trang-danh-sach-san-pham.component';

describe('TrangDanhSachSanPhamComponent', () => {
  let component: TrangDanhSachSanPhamComponent;
  let fixture: ComponentFixture<TrangDanhSachSanPhamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrangDanhSachSanPhamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrangDanhSachSanPhamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
