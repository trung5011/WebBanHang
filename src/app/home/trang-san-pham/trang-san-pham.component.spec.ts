import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrangSanPhamComponent } from './trang-san-pham.component';

describe('TrangSanPhamComponent', () => {
  let component: TrangSanPhamComponent;
  let fixture: ComponentFixture<TrangSanPhamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrangSanPhamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrangSanPhamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
