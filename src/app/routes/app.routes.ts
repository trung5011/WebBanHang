import { Routes, RouterModule} from '@angular/router';
import { TrangChuComponent } from '../home/trang-chu/trang-chu.component';
import { HomeModule } from '../home/home.module';
import { AdminModule } from '../admin/admin.module';



const routing:Routes = [
    {path:'',loadChildren:() =>HomeModule},
    {path:'home',loadChildren:() => HomeModule},
    {path:'admin',loadChildren:() => AdminModule},
    {path:'404',component:TrangChuComponent},
    {path:'**',redirectTo:'404'},
]
export const appRoutes = RouterModule.forRoot(routing);